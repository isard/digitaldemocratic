jQuery(document).ready(() => {
    base_url = `${window.location.protocol}//${window.location.host.replace(/^nextcloud\./, 'api.')}`
    $.getJSON(`${base_url}/json`, (result) => {
        if (result.logo) {
            $("#navbar-logo img").attr('src', result.logo)
        }
        if (result.product_logo) {
            $("#product-logo img").attr("src", result.product_logo)
        }
        if (result.product_url) {
            $("#product-logo a").attr("href", result.product_url)
        }
    })
    $.get(`${base_url}/header/html/nextcloud`, (result) => {
        console.log(result)
        $("#settings").before(result)
        $('#dropdownMenuAppsButton').click(() => {
            $('#dropdownMenuApps').toggle()
        })
        $('#dropdownMenuApps a').click(() => {
            $('#dropdownMenuApps').toggle()
        })
    })
    $(window).click( (event) => {
        if (
            !$(event.target).parents(
                '#dropdownMenuAppsButton, #dropdownMenuApps'
            ).length
        ) {
            $('#dropdownMenuApps').hide()
        }
    })
    $(window).blur( (event) => {
        $('#dropdownMenuApps').hide()
    })
})
