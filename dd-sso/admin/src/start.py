#!flask/bin/python
# coding=utf-8
from eventlet import monkey_patch

monkey_patch()

import json

from admin.auth.tokens import get_token_payload
from admin.lib.api_exceptions import Error
from flask import request
from flask_login import current_user, login_required
from flask_socketio import (
    SocketIO,
    close_room,
    disconnect,
    emit,
    join_room,
    leave_room,
    rooms,
    send,
)

from admin import app

app.socketio = SocketIO(app)


@app.socketio.on("connect", namespace="/sio")
@login_required
def socketio_connect():
    if current_user.id:
        join_room("admin")
        app.socketio.emit(
            "update", json.dumps("Joined admins room"), namespace="/sio", room="admin"
        )
    else:
        None


@app.socketio.on("disconnect", namespace="/sio")
def socketio_disconnect():
    leave_room("admin")


@app.socketio.on("connect", namespace="/sio/events")
def socketio_connect():
    jwt = get_token_payload(request.args.get("jwt"))

    join_room("events")
    app.socketio.emit(
        "update",
        json.dumps("Joined events room"),
        namespace="/sio/events",
        room="events",
    )


@app.socketio.on("disconnect", namespace="/sio/events")
def socketio_events_disconnect():
    leave_room("events")


if __name__ == "__main__":
    app.socketio.run(
        app,
        host="0.0.0.0",
        port=9000,
        debug=False,
    )
    # ssl_context="adhoc",
    # async_mode="threading",
    # )  # , logger=logger, engineio_logger=engineio_logger)
    # , cors_allowed_origins="*"
