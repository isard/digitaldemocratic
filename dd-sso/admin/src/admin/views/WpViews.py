#!flask/bin/python
# coding=utf-8
import json
import logging as log
import os
import socket
import sys
import time
import traceback

from flask import request

from admin import app

from .decorators import is_internal


@app.route("/api/internal/users", methods=["GET"])
@is_internal
def internal_users():
    log.error(socket.gethostbyname("isard-apps-wordpress"))
    if request.method == "GET":
        sorted_users = sorted(app.admin.get_mix_users(), key=lambda k: k["username"])
        # group_users = [user for user in sorted_users if data['path'] in user['keycloak_groups']]
        users = []
        for user in sorted_users:
            if not user.get("enabled"):
                continue
            users.append(user_parser(user))
        return json.dumps(users), 200, {"Content-Type": "application/json"}


@app.route("/api/internal/users/filter", methods=["POST"])
@is_internal
def internal_users_search():
    if request.method == "POST":
        data = request.get_json(force=True)
        users = app.admin.get_mix_users()
        result = [user_parser(user) for user in filter_users(users, data["text"])]
        sorted_result = sorted(result, key=lambda k: k["id"])
        return json.dumps(sorted_result), 200, {"Content-Type": "application/json"}


@app.route("/api/internal/groups", methods=["GET"])
@is_internal
def internal_groups():
    if request.method == "GET":
        sorted_groups = sorted(app.admin.get_mix_groups(), key=lambda k: k["name"])
        groups = []
        for group in sorted_groups:
            if not group["path"].startswith("/"):
                continue
            groups.append(
                {
                    "id": group["path"],
                    "name": group["name"],
                    "description": group.get("description", ""),
                }
            )
        return json.dumps(groups), 200, {"Content-Type": "application/json"}


@app.route("/api/internal/group/users", methods=["POST"])
@is_internal
def internal_group_users():
    if request.method == "POST":
        data = request.get_json(force=True)
        sorted_users = sorted(app.admin.get_mix_users(), key=lambda k: k["username"])
        # group_users = [user for user in sorted_users if data['path'] in user['keycloak_groups']]
        users = []
        for user in sorted_users:
            if data["path"] not in user["keycloak_groups"] or not user.get("enabled"):
                continue
            users.append(user)
        if data.get("text", False) and data["text"] != "":
            result = [user_parser(user) for user in filter_users(users, data["text"])]
        else:
            result = [user_parser(user) for user in users]
        return json.dumps(result), 200, {"Content-Type": "application/json"}


@app.route("/api/internal/roles", methods=["GET"])
@is_internal
def internal_roles():
    if request.method == "GET":
        roles = []
        for role in sorted(app.admin.get_roles(), key=lambda k: k["name"]):
            if role["name"] == "admin":
                continue
            roles.append(
                {
                    "id": role["id"],
                    "name": role["name"],
                    "description": role.get("description", ""),
                }
            )
        return json.dumps(roles), 200, {"Content-Type": "application/json"}


@app.route("/api/internal/role/users", methods=["POST"])
@is_internal
def internal_role_users():
    if request.method == "POST":
        data = request.get_json(force=True)
        sorted_users = sorted(app.admin.get_mix_users(), key=lambda k: k["username"])
        # group_users = [user for user in sorted_users if data['path'] in user['keycloak_groups']]
        users = []
        for user in sorted_users:
            if data["role"] not in user["roles"] or not user.get("enabled"):
                continue
            users.append(user)
        if data.get("text", False) and data["text"] != "":
            result = [user_parser(user) for user in filter_users(users, data["text"])]
        else:
            result = [user_parser(user) for user in users]
        return json.dumps(result), 200, {"Content-Type": "application/json"}


def user_parser(user):
    return {
        "id": user["username"],
        "first": user["first"],
        "last": user["last"],
        "role": user["roles"][0] if len(user["roles"]) else None,
        "email": user["email"],
        "groups": user["keycloak_groups"],
    }


def filter_users(users, text):
    return [
        user
        for user in users
        if text in user["username"]
        or text in user["first"]
        or text in user["last"]
        or text in user["email"]
    ]
