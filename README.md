# DD

[English](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_en.md) - [Català](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README.md) - [Castellano](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_es.md)

DD és el workspace educatiu generat en el marc del Pla de Digitalització Democràtica d’Xnet. Ha estat creat i powered per Xnet, famílies i centres promotors, IsardVDI, 3iPunt, Direcció d’Innovació Democràtica, Comissionat d’Innovació Digital, Comissionat d’Economia Social de l’Ajuntament de Barcelona, Consorci d’Educació de Barcelona. En col·laboració amb aFFaC, Escola Montseny, Escola Baixeras, Maadix, AirVPN i Wemcor. Amb l’ajuda de Miriam Carles, Cristian Ruiz, Anna Francàs, Christopher Millard. 

### Llicència

DD és una aplicació desenvolupada com a part del Pla de Digitalització Democràtica d’Xnet i famílies promotores, amb el suport de les Associacions Federades de Famílies d'Alumnes de Catalunya (aFFaC). 

L'aplicació DD ha estat realitzada per Xnet, IsardVDI i 3iPunt com a part d’un projecte pilot del Pla de Digitalització Democràtica d’Xnet financiat per la Direcció d'Innovació Democràtica, el Comissionat d’Innovació Digital de l'Ajuntament de Barcelona i el Consorci d'Educació de Barcelona, al qual han contribuït els centres participants. 

L’aplicació DD pot utilitzar-se lliurement sempre i quan consti aquest footer i es respecti la llicència AGPLv3 (https://www.gnu.org/licenses/agpl-3.0.en.html).

# Estat del projecte

Projecte en desenvolupament. En els propers mesos modificarem el repositori amb documentació més actualitzada.

## Qué es això

El projecte proporcionarà una solució integrada per a gestionar l'entorn comú en l'educació:
* Aules: Una instància de Moodle amb tema personalitzat i connectors personalitzats.
* Fitxers: Una instància del Nextcloud amb tema personalitzat i connectors personalitzats.
* Documents: Una instància d'OnlyOffice integrada amb Nextcloud.
* Pàgines web: Una instància de Wordpress amb el tema personalitzat i connectors personalitzats.
* Pad: Una instància etherpad integrada amb Nextcloud.
* Conferències: Un BigBlueButton integrat amb Moodle i Nextcloud. Necessita un servidor independent.
* Formularis: Els connectors del Nextcloud dels formularis.
* (algunes aplicacions com BigBlueButton i el email es poden integrar en el mateix servidor o servidors externs...)

  

## Requisits

Distribució de linux, **docker-ce**, and **docker-compose > 1.28**

```
docker-compose --version
# if version < 1.28: 
pip3 install --upgrade docker-compose
docker-compose --version
```

Si fas servir una distribució **debian**, per fer la instal·lació de les dependencies, docker i docker-compose pots seguir: [sysadm/debian_docker_and_compose.sh](sysadm/debian_docker_and_compose.sh)


## Inici ràpid

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Canvia les contrasenyes per defecte

```
./securize_conf.sh
```

Editeu les variables del fitxer digitaldemocratic.conf per satisfer les vostres necessitats.

```
cp -R custom.sample custom
```

Edita i substitueix els fitxers per personalitzar el sistema.

La primera vegada executa:
```
./dd-ctl repo-update
```

I després:
```
./dd-ctl all
```
NOTA: L'autenticació SAML actualment es troba automatitzada:

- Moodle: No completament automatitzat.
  1. Inicieu la sessió a Moodle com a administrador via: https://moodle.\<domain\>/login/index.php?saml=off
  2. Aneu a la configuració d'autenticació: https://moodle.\<domain\>/admin/settings.php?section=manageauths
  3. Activa SAML2 fent clic a l'ull.
  4. Clic a *configuració* a SAML2
  5. Feu clic al botó *Regenera el certificat* dins del formulari. Després d'això, torna a la pàgina de configuració de SAML2.
  6. Feu clic al botó Bloqueja el *certificat*.
  7. Al terminal executeu l'script per autoconfigure: acoblador exec isard-sso-admin python3 moodle_saml.py
  8. L'última cosa és purgar la memòria cau de moodle: ]]femida l'script php-fpm7 de l'acoblador Exec, feu-ho a través de moodle ui]]
- Nextcloud: Automatitzada. Després d'acabar el *make all* hauria d'estar llest. En cas que falli refereix a isard-sso/docs.
- Wordpress: Automatitzada. Després d'acabar el *make all* hauria d'estar llest. En cas que falli refereix a isard-sso/docs.


## instruccions de post instal·lació

Podeu trobar un manual pas a pas a: (https://digitaldemocratic.gitlab.io/digitaldemocratic).



## Instal.lació estesa

Podeu iniciar aquest projecte en qualsevol servidor amb docker &  docker-compose (qualsevol sistema operatiu hauria de funcionar). Per a instal·lar aquests paquets a la vostra distribució, consulteu el funcionament de docker & docker-compose a la documentació oficial i a la carpeta sysadm teniu  alguns scripts d'automatització

Qualsevol distribució hauria de funcionar però,  si voleu utilitzar els nostres scripts sysadm per instal·lar docker & docker-compose , utilitzeu Debian Buster (10).

### Clonar els submòduls

```
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive
```

### docker

Referiu-vos a la documentació oficial (https://docs.docker.com/engine/install/) o utilitzeu l'script a la carpeta sysadm per a Debian Buster (10).

### docker-compose

Referiu-vos a la documentació oficial (https://docs.docker.com/compose/install/) o utilitzeu l'script a la carpeta sysadm per a Debian Buster (10).

### Configuració

Copieu digitaldemocratic.conf.exemple a digitaldemocratic.conf i editeu-lo per satisfer les vostres necessitats. Com a mínim (per a desenvolupament) heu d'adaptar la variable DOMAIN al vostre domini arrel.

- PRODUCCIÓ: Necessiteu un dns multidomini (o redirigeix els subdominis múltiples) a la vostra màquina d'amfitrió.
- Desenvolupament: Editeu el fitxer /etc/hosts i afegiu els subdominis per a propòsits de proves locals.

#### Subdominis

- Keycloak: sso.<yourdomain.org>
- Admin: admin.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>
- (opcional) FreeIPA: ipa.<yourdomain.org>

### Personalització

Copia recursivament la carpeta *custom.sample* a *custom* i edita els fitxers yaml de personalització i menú i substitueix les imatges.

### Iniciar el projecte

La primera vegada (i si voleu actualitzar a la última versió posteriorment) executeu:

```
./dd-ctl repo-update
```

I després:

```
./dd-ctl all
```

Posteriorment podreu iniciar o aturar amb:

```
./dd-ctl down
./dd-ctl up
```


### Integració

Llegiu el fitxer [SAML_README.md](https://gitlab.com/isard/isard-sso/-/blob/master/docs/SAML_README.md) a la carpeta isard-sso/docs per integrar totes les aplicacions. Ara el  Nextcloud i el Wordpress s'haurien d'integrar automàticament amb el Keycloak.

Vegeu també les [Intruccions post-instal.lació](https://digitaldemocratic.gitlab.io/digitaldemocratic/post-install.ca/).
