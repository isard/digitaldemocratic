#!/usr/bin/env python
# coding=utf-8
import json
import logging
import os
import pprint
import time
import traceback
from datetime import datetime, timedelta

import yaml
from api import app as application
from keycloak import KeycloakAdmin


class Avatars:
    def __init__(
        self,
        url="http://isard-sso-keycloak:8080/auth/",
        username=os.environ["KEYCLOAK_USER"],
        password=os.environ["KEYCLOAK_PASSWORD"],
        realm="master",
        verify=True,
    ):
        self.url = url
        self.username = username
        self.password = password
        self.realm = realm
        self.verify = verify

    def connect(self):
        self.keycloak_admin = KeycloakAdmin(
            server_url=self.url,
            username=self.username,
            password=self.password,
            realm_name=self.realm,
            verify=self.verify,
        )

    def get_user_avatar(self, username):
        self.connect()
        return self.keycloak_admin.get_user_id(username)


# # Add user
# new_user = keycloak_admin.create_user({"email": "example@example.com",
#                     "username": "example@example.com",
#                     "enabled": True,
#                     "firstName": "Example",
#                     "lastName": "Example"})
# print(new_user)
