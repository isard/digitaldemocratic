# IsardVDI - OpenID infrastructure

**NOTE**: This repo now is included in https://gitlab.com/digitaldemocratic/digitaldemocratic that is the repo that includes this repo. Maybe this SSO repo won't work alone by now...


This will bring up a full OpenID auth infrastructure consisting in this hosts from $DOMAIN var set in main.conf:

- FreeIPA: https://ipa.$DOMAIN
- Mokey: https://login.$DOMAIN
- Hydra: https://hydra.$DOMAIN

NOTE: If you use the default example domain in main.conf.example you will need to add this domain mapping to IP in your hosts file at clients.

# Quick start

1. Edit main.conf (to suit your needs). If you set Letsencrypt vars it will ask and renew the certificate automatically.
2. Build docker.compose.yml: ```./build-compose.sh```
3. Bring containers up: ```docker-compose up -d```

    Wait till freeipa container is ready (it can take SEVERAL minutes the first time): ```docker logs freeipa --follow```

    And wait to be ready. It will log: *FreeIPA server configured.*

4. Add mokey client in freeipa: ```docker exec freeipa /bin/sh -c "scripts/mokey.sh"```
5. check that mokey is up: ```docker logs mokey --follow```. It will log: *⇨ http server started on [::]:8080*

Access your IP/DNS and login page should come up.

# Firewall

You should open 80 and 443

# Add client apps

## The easy way

[**Not working yet! Refer to 'Do it yourself'**]

For most client apps the default script will be enough:

```
docker exec \
    -e APP_ID=<app name> \
    -e APP_SECRET=<app secret> \
    -e APP_CALLBACKS=<app callback url wiht https://...> \
    hydra /bin/sh -c "scripts/add_app.sh"
```

## Do it yourself
For example we will be creating a moodle app client. You need to adapt the vars to your app

```
DOMAIN=(your domain root as set in main.conf)
APP_ID=moodle
APP_SECRET=Sup3rS3cr3t
```
```
docker-compose  exec hydra \
    hydra clients create \
    --endpoint http://hydra:4445/ \
    --id $APP_ID \
    --secret $APP_SECRET \
    --grant-types client_credentials,authorization_code,refresh_token \
    --token-endpoint-auth-m01d4f1c0-8a53-4df5-8a46-de670f42a4dfethod client_secret_post \
    --response-types code \
    --scope openid,offline,profile,email \
    --callbacks https://moodle.${DOMAIN}/auth/oidc/
```

And then validate the app:
```
docker-compose exec hydra \
    hydra token client \
    --endpoint http://hydra:4444/ \
    --client-id $APP_ID \
    --client-secret $APP_SECRET
```

## Example configuration for Moodle OpenID Connect

- authendpoint:       https://hydra.${DOMAIN}/oauth2/auth
- tokenendpoint:      https://hydra.${DOMAIN}/oauth2/token
- oidcresource:       https://hydra.${DOMAIN}/userinfo
- oidcscope:          openid profile email
- single_sign_off:    [checked]
- logouturi:          https://login.${DOMAIN}/auth/logout

## Example configuration for Nextcloud Custom OpenID Connect
- Prevent creating an account if the email address exists
- Update user profile every login
- Do not prune not available user groups on login
- automatically create groups if they do not exist

- Authorize url:       		https://hydra.${DOMAIN}/oauth2/auth
- Token url:      		https://hydra.${DOMAIN}/oauth2/token
- User info URL (optional):     https://hydra.${DOMAIN}/userinfo
- Scope:		        openid profile offline email
- Logout URL (optional):        https://login.${DOMAIN}/auth/logout

### Be aware on nextcloud behind proxy
Behind proxy we should force nextcloud-app to use https. From documentation it should be NEXTCLOUD_OVERWRITEPROTOCOL but it is not:
- https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/reverse_proxy_configuration.html

Instead we used in docker/nextcloud/nextcloud.yml compose file the envvar NC_overwriteprotocol as stated in this thread:
- https://github.com/nextcl01d4f1c0-8a53-4df5-8a46-de670f42a4dfoud/docker/pull/819

Extra information about this:
Some users may get strange reply(Callback) url error from provider even if you pasted the right url, that's because your nextcloud 
server may generate http urls when you are actually using https. Please set 'overwriteprotocol' => 'https', in your config.php file.

<?php
$CONFIG = array (
  'overwriteprotocol' => 'https',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 

## Nexcloud autoredirect for unauthorized users
Set social_login_auto_redirect and social_login_http_client in config.php
- https://apps.nextcloud.com/apps/sociallogin

### APPENDIX: Nextcloud Social Login plugin


Config

You can use 'social_login_auto_redirect' => true setting in config.php for auto redirect unauthorized users to social login if only one 
provider is configured. If you want to temporary disable this function (e.g. for login as local admin), you can add noredir=1 query 
parameter in url for login page. Something like https://cloud.domain.com/login?noredir=1

To set timeout for http client, you can use

  'social_login_http_client' => [
    'timeout' => 45,
  ],

# IsardVDI office apps

Refer to https://gitlab.com/isard/isard-office repository for sample moodle, nextcloud, jitsi (and more) apps

# Troubleshooting

## FreeIPA

ldapsearch -x -b "dc=domain,dc=org" -H ldap://ipa.domain.org
ldapsearch -x -b "dc=domain,dc=org" -H ldap://ipa.domain.org -D "uid=admin,cn=users,cn=compat,dc=domain,dc=org" -W 


# KEYCLOACK

Here’s a list of OIDC endpoints that the Keycloak publishes. These URLs are useful if you are using a non-Keycloak client adapter to talk OIDC with the auth server. These are all relative URLs and the root of the URL being the HTTP(S) protocol, hostname, and usually path prefixed with /auth: i.e. https://localhost:8080/auth

/realms/{realm-name}/protocol/isard-sso-connect/token

    This is the URL endpoint for obtaining a temporary code in the Authorization Code Flow or for obtaining tokens via the Implicit Flow, Direct Grants, or Client Grants.
/realms/{realm-name}/protocol/isard-sso-connect/auth

    This is the URL endpoint for the Authorization Code Flow to turn a temporary code into a token.
/realms/{realm-name}/protocol/isard-sso-connect/logout

    This is the URL endpoint for performing logouts.
/realms/{realm-name}/protocol/isard-sso-connect/userinfo

    This is the URL endpoint for the User Info service described in the OIDC specification.

In all of these replace {realm-name} with the name of the realm.


http://login.mydomain.duckns.org/auth/realms/master/protocol/isard-sso-connect/logout
