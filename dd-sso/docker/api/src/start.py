#!flask/bin/python
# coding=utf-8
from eventlet import monkey_patch

monkey_patch()

from api import app
from flask_socketio import SocketIO

socketio = SocketIO(app)

if __name__ == "__main__":
    import logging

    logger = logging.getLogger("socketio")
    logger.setLevel("ERROR")
    engineio_logger = logging.getLogger("engineio")
    engineio_logger.setLevel("ERROR")

    socketio.run(app, host="0.0.0.0", port=7039, debug=False)
