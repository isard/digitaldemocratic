#!/usr/bin/env python
# coding=utf-8
import json
import logging as log

from lib.keycloak_client import KeycloakClient


class KeycloakConfig:
    def __init__(self):
        self.keycloak = KeycloakClient()

    def config_realm_update(self, path_json="/admin/custom/keycloak/realm.json"):
        self.keycloak.connect()
        k = self.keycloak.keycloak_admin

        with open(path_json) as json_file:
            d_update = json.load(json_file)
            k.update_realm("master", d_update)

    def config_role_list(self):
        self.keycloak.connect()
        k = self.keycloak.keycloak_admin

        name_protocol_mapper = "role list"
        id_client_scope_role_list = [
            a["id"] for a in k.get_client_scopes() if a["name"] == "role_list"
        ][0]
        d = k.get_client_scope(id_client_scope_role_list)
        d_mapper = [
            a for a in d["protocolMappers"] if a["name"] == name_protocol_mapper
        ][0]
        id_mapper = d_mapper["id"]

        # Single Role Attribute = On
        d_mapper["config"]["single"] = "true"

        k.update_mapper_in_client_scope(id_client_scope_role_list, id_mapper, d_mapper)


if __name__ == "__main__":
    keycloack_config = KeycloakConfig()
    keycloack_config.config_realm_update()
    keycloack_config.config_role_list()
