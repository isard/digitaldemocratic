# coding=utf-8

import os

from api import app
from flask import Response, jsonify, redirect, render_template, request, url_for

from .decorators import is_internal


@app.route("/restart", methods=["GET"])
@is_internal
def api_restart():
    os.system("kill 1")


# @app.route('/user_menu/<format>', methods=['GET'])
# @app.route('/user_menu/<format>/<application>', methods=['GET'])
# def api_v2_user_menu(format,application=False):
#     if application == False:
#         if format == 'json':
#             if application == False:
#                 return json.dumps(menu.get_user_nenu()), 200, {'Content-Type': 'application/json'}
