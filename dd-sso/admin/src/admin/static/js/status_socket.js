notice={}
$lost=0;

socket = io.connect(location.protocol+'//' + document.domain +'/sio');
console.log(location.protocol+'//' + document.domain +'/sio')
socket.on('connect', function() {
    if($lost){location.reload();}
    console.log('Listening status socket');
});

socket.on('connect_error', function(data) {
    $lost=$lost+1;
    $('#modal-lostconnection').modal({
        backdrop: 'static',
        keyboard: false
    }).modal('show');
});

socket.on('notify-create', function(data) {
    var data = JSON.parse(data);
    notice[data.id] = new PNotify({
        title: data.title,
        text: data.text,
        hide: false,
        type: data.type
    });
});

socket.on('notify-destroy', function(data) {
    var data = JSON.parse(data);
    notice[data.id].remove()
});

socket.on('notify-increment', function(data) {
    var data = JSON.parse(data);
    if(!( data.id in notice)){
        notice[data.id] = new PNotify({
            title: data.title,
            text: data.text,
            hide: false,
            type: data.type
        });
    }
    // console.log(data.text)
    notice[data.id].update({
        text: data.text
    })
    if(! data.table == false){
        dtUpdateInsert(data.table,data['data']['data'])
    }
});



    // new PNotify({
    //     title: "Quota for creating desktops full.",
    //     text: "Can't create another desktop, user quota full.",
    //     hide: true,
    //     delay: 3000,
    //     icon: 'fa fa-alert-sign',
    //     opacity: 1,
    //     type: 'error'
    // });

// socket.on('update', function(data) {
//     var data = JSON.parse(data);
//     console.log('Status update')
//     console.log(data)
//     // var data = JSON.parse(data);
//     // drawUserQuota(data);
// });

socket.on('update', function(data) {
    var data = JSON.parse(data);
    console.log('Status update')
    console.log(data)
    // var data = JSON.parse(data);
    // drawUserQuota(data);
});

    // {'event':'traceback',
    // 'id':u['id'],
    // 'item':'group',
    // 'action':'add'
    // 'name':g['name'],
    // 'progress':str(item)+'/'+str(total),
    // 'status':False,
    // 'msg':,
    // 'payload':{'traceback':traceback.format_exc(),
    //             'data':g})

socket.on('progress', function(data) {
    var data = JSON.parse(data);
    console.log(data)
    // $('.modal-progress #item').html(data.item)
});



//// 
