#!/bin/sh
echo "Domain(s) $LETSENCRYPT_DNS renewed. Restarting haproxy..."
    cat /etc/letsencrypt/live/$LETSENCRYPT_DNS/fullchain.pem /etc/letsencrypt/live/$LETSENCRYPT_DNS/privkey.pem > /certs/chain.pem
    chmod 440 /certs/chain.pem
    mkdir -p /certs/letsencrypt/$LETSENCRYPT_DNS
    cp /etc/letsencrypt/live/$LETSENCRYPT_DNS/* /certs/letsencrypt/$LETSENCRYPT_DNS/

kill -SIGUSR2 1
