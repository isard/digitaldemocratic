# Welcome

- [Post-installation steps (in Catalan)](./post-install.ca/)
- [Project Management](./project-management/)



This site is built by [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs). You can [browse its source code](https://gitlab.com/digitaldemocratic/digitaldemocratic).
