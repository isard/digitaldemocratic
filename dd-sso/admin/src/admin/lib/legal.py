import logging as log
import os
import traceback

from admin import app
from pprint import pprint

from minio import Minio
from minio.commonconfig import REPLACE, CopySource
from minio.deleteobjects import DeleteObject
from requests import get, post

legal_path= os.path.join(app.root_path, "static/templates/pages/legal/")

def get_legal(lang):
    with open(legal_path+lang, "r") as languagefile:
        return languagefile.read()

def gen_legal_if_not_exists(lang):
    if not os.path.isfile(legal_path+lang):
        log.debug("Creating new language file")
        with open(legal_path+lang, "w") as languagefile:
            languagefile.write("<b>Legal</b><br>This is the default legal page for language " + lang)

def new_legal(lang,html):
    with open(legal_path+lang, "w") as languagefile:
        languagefile.write(html)