#!flask/bin/python
# coding=utf-8

import json
import logging as log
import os
import socket
from functools import wraps

from flask import redirect, request, url_for
from flask_login import current_user, logout_user
from jose import jwt

from ..auth.tokens import get_header_jwt_payload


def is_admin(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if current_user.role == "admin":
            return fn(*args, **kwargs)
        return redirect(url_for("login"))

    return decorated_view


def is_internal(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        remote_addr = (
            request.headers["X-Forwarded-For"].split(",")[0]
            if "X-Forwarded-For" in request.headers
            else request.remote_addr.split(",")[0]
        )
        ## Now only checks if it is wordpress container,
        ## but we should check if it is internal net and not haproxy
        if socket.gethostbyname("isard-apps-wordpress") == remote_addr:
            return fn(*args, **kwargs)
        return (
            json.dumps(
                {
                    "error": "unauthorized",
                    "msg": "Unauthorized access",
                }
            ),
            401,
            {"Content-Type": "application/json"},
        )

    return decorated_view


def has_token(fn):
    @wraps(fn)
    def decorated(*args, **kwargs):
        payload = get_header_jwt_payload()
        return fn(*args, **kwargs)

    return decorated


def is_internal_or_has_token(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        remote_addr = (
            request.headers["X-Forwarded-For"].split(",")[0]
            if "X-Forwarded-For" in request.headers
            else request.remote_addr.split(",")[0]
        )
        payload = get_header_jwt_payload()

        if socket.gethostbyname("isard-apps-wordpress") == remote_addr:
            return fn(*args, **kwargs)
        payload = get_header_jwt_payload()
        return fn(*args, **kwargs)

    return decorated_view


def login_or_token(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if current_user.is_authenticated:
            return fn(*args, **kwargs)
        payload = get_header_jwt_payload()
        return fn(*args, **kwargs)

    return decorated_view
