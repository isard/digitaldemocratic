#!flask/bin/python
# coding=utf-8

import socket
from functools import wraps

from flask import redirect, request, url_for


def is_internal(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        remote_addr = (
            request.headers["X-Forwarded-For"].split(",")[0]
            if "X-Forwarded-For" in request.headers
            else request.remote_addr.split(",")[0]
        )
        if socket.gethostbyname("isard-sso-admin") == remote_addr:
            return fn(*args, **kwargs)
        return ""

    return decorated_view
