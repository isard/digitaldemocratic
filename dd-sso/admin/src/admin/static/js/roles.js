
$(document).on('shown.bs.modal', '#modalAddDesktop', function () {
    modal_add_desktops.columns.adjust().draw();
}); 

$(document).ready(function() {

	$('.btn-global-resync').on('click', function () {
		$.ajax({
			type: "GET",
			url:"/api/resync",
			success: function(data)
			{
				table.ajax.reload();
				// $("#modalImport").modal('hide');
				// users_table.ajax.reload();
				// groups_table.ajax.reload();
			},
			error: function(data)
			{
				alert('Something went wrong on our side...')
			}
		});
	});
	
	$('.btn-new').on('click', function () {
            $("#modalAdd")[0].reset();
            $('#modalAddDesktop').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show');
            $('#modalAdd').parsley();
	});

	//DataTable Main renderer
	var table = $('#roles').DataTable({
			"ajax": {
				"url": "/api/roles",
				"dataSrc": ""
			},
			"language": {
				"loadingRecords": '<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                "emptyTable": "<h1>You don't have any role created yet.</h1><br><h2>Create one using the +Add new button on top right of this page.</h2>"
			},           
			"rowId": "id",
			"deferRender": true,
			"columns": [
                { "data": "id", "width": "10px" },
                { "data": "name", "width": "10px" },
				],
			 "order": [[1, 'asc']],
			//  "columnDefs": [ {
			// 	"targets": 0,
			// 	"render": function ( data, type, full, meta ) {
			// 		// return '<object data="/static/img/missing.jpg" type="image/jpeg" width="25" height="25"><img src="/avatar/'+full.id+'" title="'+full.id+'" width="25" height="25"></object>'
			// 		return '<img src="/avatar/'+full.name+'" title="'+full.name+'" width="25" height="25" onerror="if (this.src != \'/static/img/missing.jpg\') this.src = \'/static/img/missing.jpg\';">'
			// 	}}]
	} );
});