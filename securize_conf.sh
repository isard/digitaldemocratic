#!/bin/sh -eu

cd "$(dirname "$0")"
./dd-ctl securize

cat >&2 <<EOF

This script will be removed!
Please run './dd-ctl securize' in the future.
EOF
