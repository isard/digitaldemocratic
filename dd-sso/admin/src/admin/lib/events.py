#!flask/bin/python
# coding=utf-8
import base64
import json
import logging as log
import os
import sys
import traceback
from time import sleep
from uuid import uuid4

from flask import Response, jsonify, redirect, render_template, request, url_for
from flask_socketio import (
    SocketIO,
    close_room,
    disconnect,
    emit,
    join_room,
    leave_room,
    rooms,
    send,
)

from admin import app


def sio_event_send(event, data):
    app.socketio.emit(
        event,
        json.dumps(data),
        namespace="/sio/events",
        room="events",
    )
    sleep(0.001)


class Events:
    def __init__(self, title, text="", total=0, table=False, type="info"):
        # notice, info, success, and error
        self.eid = str(base64.b64encode(os.urandom(32))[:8])
        self.title = title
        self.text = text
        self.total = total
        self.table = table
        self.item = 0
        self.type = type
        self.create()

    def create(self):
        log.info("START " + self.eid + ": " + self.text)
        app.socketio.emit(
            "notify-create",
            json.dumps(
                {
                    "id": self.eid,
                    "title": self.title,
                    "text": self.text,
                    "type": self.type,
                }
            ),
            namespace="/sio",
            room="admin",
        )
        sleep(0.001)

    def __del__(self):
        log.info("END " + self.eid + ": " + self.text)
        app.socketio.emit(
            "notify-destroy",
            json.dumps({"id": self.eid}),
            namespace="/sio",
            room="admin",
        )
        sleep(0.001)

    def update_text(self, text):
        self.text = text
        app.socketio.emit(
            "notify-update",
            json.dumps(
                {
                    "id": self.eid,
                    "text": self.text,
                }
            ),
            namespace="/sio",
            room="admin",
        )
        sleep(0.001)

    def append_text(self, text):
        self.text = self.text + "<br>" + text
        app.socketio.emit(
            "notify-update",
            json.dumps(
                {
                    "id": self.eid,
                    "text": self.text,
                }
            ),
            namespace="/sio",
            room="admin",
        )
        sleep(0.001)

    def increment(self, data={"name": "", "data": []}):
        self.item += 1
        log.info("INCREMENT " + self.eid + ": " + self.text)
        app.socketio.emit(
            "notify-increment",
            json.dumps(
                {
                    "id": self.eid,
                    "title": self.title,
                    "text": "["
                    + str(self.item)
                    + "/"
                    + str(self.total)
                    + "] "
                    + self.text
                    + " "
                    + data["name"],
                    "item": self.item,
                    "total": self.total,
                    "table": self.table,
                    "type": self.type,
                    "data": data,
                }
            ),
            namespace="/sio",
            room="admin",
        )
        sleep(0.0001)

    def decrement(self, data={"name": "", "data": []}):
        self.item -= 1
        log.info("DECREMENT " + self.eid + ": " + self.text)
        app.socketio.emit(
            "notify-decrement",
            json.dumps(
                {
                    "id": self.eid,
                    "title": self.title,
                    "text": "["
                    + str(self.item)
                    + "/"
                    + str(self.total)
                    + "] "
                    + self.text
                    + " "
                    + data["name"],
                    "item": self.item,
                    "total": self.total,
                    "table": self.table,
                    "type": self.type,
                    "data": data,
                }
            ),
            namespace="/sio",
            room="admin",
        )
        sleep(0.001)

    def reload(self):
        app.socketio.emit("reload", json.dumps({}), namespace="/sio", room="admin")
        sleep(0.0001)

    def table(self, event, table, data={}):
        # refresh, add, delete, update
        app.socketio.emit(
            "table_" + event,
            json.dumps({"table": table, "data": data}),
            namespace="/sio",
            room="admin",
        )
        sleep(0.0001)
